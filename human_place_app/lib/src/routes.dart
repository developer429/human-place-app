import 'package:flutter/material.dart';
import 'package:human_place_app/src/screens/chat_screen.dart';
import 'package:human_place_app/src/screens/intro_slider.dart';
import 'package:human_place_app/src/screens/login_screen.dart';
import 'package:human_place_app/src/screens/splash_screen.dart';


final routes = {
  SplashScreen.routerName: (BuildContext context) => SplashScreen(),
  IntroSlider.routerName : (BuildContext context) => IntroSlider(),
  ChatScreen.routerName : (BuildContext context) => ChatScreen(),
  LoginScreen.routerName: (BuildContext context) => LoginScreen(),
};
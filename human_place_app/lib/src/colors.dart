import 'package:flutter/material.dart';

const Color primary = Color(0xFF6500E8);
const Color secondary = Color(0xFFffb740);
const Color gradientBlue = Color(0xFF61a8d8);
const Color gradientGreen = Color(0xFF00c69c);
const Color grey = Color(0xFF4b4e55);

class CustomColors {
  static const Color primary = Color(0xFF6500E8);
  static const Color orange = Color(0xFFFF8453);
  static const Color lightBlue = Color(0xFFa4d4fd);
  static const Color purple = Color(0xFFA060ff);
}
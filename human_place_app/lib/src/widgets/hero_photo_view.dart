import 'package:flutter/material.dart';
import 'package:human_place_app/src/colors.dart';
import 'package:photo_view/photo_view.dart';

class HeroPhotoView extends StatelessWidget {
  const HeroPhotoView({
    this.url,
  });

  final String url;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(
        height: MediaQuery.of(context).size.height,
      ),
      child: PhotoView(
        imageProvider: NetworkImage(url),
        loadFailedChild: Container(
          height: 50,
          child: CircularProgressIndicator(
            backgroundColor: primary,
          ),
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:human_place_app/src/services/api_service.dart';
import 'package:human_place_app/src/widgets/hero_photo_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_api/youtube_api.dart';

import '../colors.dart';

class ItemMessage extends StatefulWidget {
  bool userMessage;
  String content;
  String type;
  YT_API dataVideo;

  ItemMessage({
    this.userMessage: false,
    this.content,
    this.type,
    this.dataVideo,
  });

  @override
  _ItemMessageState createState() => _ItemMessageState();
}

class _ItemMessageState extends State<ItemMessage> {

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (widget.type) {
      case 'text':
        return Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: widget.userMessage
                ? MainAxisAlignment.end
                : MainAxisAlignment.start,
            crossAxisAlignment: widget.userMessage
                ? CrossAxisAlignment.end
                : CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: widget.userMessage ? size.width * 0.3 : 15,
              ),
              Flexible(
                child: Container(
                  padding: EdgeInsets.symmetric(
                          vertical: 8,
                          horizontal: 16,
                        ),
                  decoration: BoxDecoration(
                    color:
                        widget.userMessage ? CustomColors.primary : Colors.white,
                    borderRadius: widget.userMessage
                        ? BorderRadius.only(
                            topRight: Radius.circular(8),
                            topLeft: Radius.circular(8),
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(0),
                          )
                        : BorderRadius.only(
                      topRight: Radius.circular(8),
                      topLeft: Radius.circular(8),
                      bottomLeft: Radius.circular(0),
                      bottomRight: Radius.circular(8),
                    ),
                  ),
                  child: Text(
                    widget.content,
                    style: TextStyle(
                        color: widget.userMessage ? Colors.white : Colors.black,
                        fontSize: 16,
                      fontFamily: 'roboto-regular'
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: widget.userMessage ? 15 : size.width * 0.3,
              ),
            ],
          ),
        );
      case 'image':
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    HeroPhotoView(
                      url: widget.content,
                    ),
              ),
            );
          },
          child: Container(
            margin: EdgeInsets.only(left: 20),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                height: size.height * 0.25,
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl: widget.content,
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
        );
      case 'video':
        return InkWell(
          onTap: () {
            final api = APIService.instance;
            _launchURL(api.getURL(widget.dataVideo.kind, widget.dataVideo.id));
          },
          child: Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Column(
              children: <Widget>[
                ListTile(
                  isThreeLine: true,
                  title: Text(
                    'YouTube',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        widget.dataVideo.title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        widget.dataVideo.description,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      height: size.height * 0.25,
                      width: size.width * 0.80,
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: widget.dataVideo.thumbnail['high']['url'],
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );

      default:
        {
          return Container();
        }
    }
  }
}

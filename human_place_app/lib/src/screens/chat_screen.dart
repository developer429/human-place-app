import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/utils/language.dart';
import 'package:flutter_dialogflow/v2/auth_google.dart';
import 'package:flutter_dialogflow/v2/dialogflow_v2.dart';
import 'package:human_place_app/src/colors.dart';
import 'package:human_place_app/src/services/api_service.dart';
import 'package:human_place_app/src/widgets/item_messaage.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;
import 'package:youtube_api/youtube_api.dart';

class ChatScreen extends StatefulWidget {
  static final routerName = '/chat-screen';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final List<ItemMessage> _messages = [];
  final TextEditingController _textController = TextEditingController();
  String action = 'quickly_answers';
  bool panelOpen = false;
  bool initialPanel = true;
  String _sessionId = '';
  //double customSizeBottomWidget;
  List optionsReal = [];

  @override
  void initState() {
    _sessionId = Uuid().v4();
    super.initState();
  }

  void _sendLogToServer(String data) async {
    final response = await http.post(
      'https://4v4q5epsi7.execute-api.us-east-2.amazonaws.com/dev/processIntent',
      body: data,
    );
    print(response.body);
  }

  void _sendQueryBot(String query, BuildContext context) async {
    _textController.clear();
    AuthGoogle authGoogle =
    await AuthGoogle(fileJson: "assets/gcp.json", sessionId: _sessionId)
        .build();
    Dialogflow dialogflow = Dialogflow(
        authGoogle: authGoogle, language: Language.spanishLatinAmerica);
    print('Esperando respuesta del api');
    AIResponse response = await dialogflow.detectIntent(query);

    _sendLogToServer(response.raw);

    final messages = response
        .getListMessage()
        .where((element) => element is Map && element.containsKey('text'))
        .map((element) => element['text']['text'].first)
        .toList();

    final payload = response
        .getListMessage()
        .where((element) => element is Map && element.containsKey('payload'));
    var videoUrl;
    var imageUrl;
    var options;

    if (payload.isNotEmpty) {
      videoUrl = payload.first['payload']['video'] ?? [];
      imageUrl = payload.first['payload']['image'] ?? [];
      options = List<String>.from(payload.first['payload']['options'] ?? []);

      if (options.length != 0) {
        // Condicionar altura del bottom del chat para que las
        // respuestas rapidas no queden debajo del modal
        /*if (options.length < 5) {
          customSizeBottomWidget = MediaQuery.of(context).size.height * 0.1;
          setState(() {});
        } else if (options.length < 10) {
          customSizeBottomWidget = MediaQuery.of(context).size.height * 0.2;
          setState(() {});
        } else if (options.length > 9) {
          customSizeBottomWidget = MediaQuery.of(context).size.height * 0.3;
          setState(() {});
        } else if (options.length > 14) {
          customSizeBottomWidget = MediaQuery.of(context).size.height * 0.4;
          setState(() {});
        } else if (options.length > 19) {
          customSizeBottomWidget = MediaQuery.of(context).size.height * 0.5;
          setState(() {});
        }*/
        if (videoUrl.length != 0) {
          YT_API video =await APIService.instance.fetchVideo(urlVideo: videoUrl);
          _messages.add(
              ItemMessage(
                type: 'video',
                dataVideo: video,
              )
          );
        }
        if (imageUrl.length != 0) {
          _messages.add(
              ItemMessage(
                type: 'image',
                content: imageUrl,
              )
          );
        }

        print(options);
        setState(() {
          optionsReal = options;
        });
        closePanel(context);
        action = 'quickly_answers';
        //quicklyAnswers(context, options: options);
      }  else {
        if (videoUrl.length != 0) {
          YT_API video =await APIService.instance.fetchVideo(urlVideo: videoUrl);
          _messages.add(
              ItemMessage(
                type: 'video',
                dataVideo: video,
              )
          );
        }
        if (imageUrl.length != 0) {
          _messages.add(
              ItemMessage(
                type: 'image',
                content: imageUrl,
              )
          );
        }
        action = 'text';
        closePanel(context);
      }
    } else {
      action = 'text';
      closePanel(context);
    }

    setState(() {
      _messages.addAll(messages.map((text) => ItemMessage(content: text, type: 'text',)));
    });

    print(response.getListMessage());
  }

  @override
  Widget build(BuildContext context) {
    //Mostrar la vista con la respuesta rapida al inicio
    if(initialPanel){
      quicklyAnswers(context, options: ['iniciar SAM']);
    }
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: CustomColors.lightBlue,
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 14,),
                Container(
                  width: size.width,
                  padding: EdgeInsets.only(left: 15),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: CustomColors.primary,
                        shape: BoxShape.circle,
                      ),
                      child: Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage('assets/logos/logotipo-huma-place.png')
                              )
                          )
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    reverse: true,
                    itemBuilder: (_, int index) {
                      var _msgsR = new List.from(_messages.reversed);
                      return index == 0
                          ? Container(
                              margin: EdgeInsets.only(bottom: 70),
                              child: _msgsR[index],
                            )
                          : _msgsR[index];
                    },
                    itemCount: _messages.length,
                  ),
                ),
                _buildBottomWidget(
                  answerType: action,
                  context: context,
                  options: optionsReal
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void waitingSam(context) {
    panelOpen = true;
    showModalBottomSheet(
        barrierColor: Colors.black.withAlpha(1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        backgroundColor: Color(0xFF7B9EBD),
        enableDrag: false,
        isDismissible: false,
        elevation: 1,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: 50,
          );
        });
  }

  void quicklyAnswers(context, {List options}) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      panelOpen = true;
      showModalBottomSheet(
          barrierColor: Colors.black.withAlpha(1),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          backgroundColor: Color(0xFF5090A6),
          enableDrag: false,
          isDismissible: false,
          elevation: 1,
          context: context,
          builder: (BuildContext bc) {
            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(12),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 12,
                  runSpacing: 20,
                  children: List<Widget>.generate(
                    options.length,
                        (index) => InkWell(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      onTap: () {
                        closePanel(context);
                        setState(() {
                          if (options.first == 'iniciar SAM') {
                            action = 'waiting_sam';
                            initialPanel = false;
                            waitingSam(context);
                            _messages.add(
                              ItemMessage(
                                userMessage: true,
                                content: options[index],
                                type: 'text',
                              ),
                            );
                          } else {
                            closePanel(context);
                            action = 'waiting_sam';
                            waitingSam(context);
                            _messages.add(
                              ItemMessage(
                                userMessage: true,
                                content: options[index],
                                type: 'text',
                              ),
                            );
                          }
                        });
                        _sendQueryBot(options[index], context);
                      },
                      child: new Container(
                        padding:
                        EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Text(
                          options[index],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          });
    });
  }

  void closePanel(context) {
    if (panelOpen) {
      Navigator.of(context).pop();
    }
  }

  Widget _buildBottomWidget({
    String answerType,
    BuildContext context,
    List options
  }) {
    return action == 'text'
        ? Container(
            color: Color(0xFFd6d6d6),
            child: Row(
              children: <Widget>[
                SizedBox(width: 8,),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 8
                    ),
                    child: TextField(
                      autofocus: true,
                      controller: _textController,
                      maxLines: null,
                      cursorColor: primary,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                        hintText: 'Mensaje',
                        hintStyle: TextStyle(
                          color: Colors.black87,
                          fontFamily: 'roboto-regular'
                        ),
                        isDense: true,
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.all(7),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              18,
                            ),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              18,
                            ),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              18,
                            ),
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              18,
                            ),
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              18,
                            ),
                          ),
                        ),
                      ),
                      style: TextStyle(
                        color: Colors.black87,
                        fontFamily: 'roboto-regular'
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 10,
                  ),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        action = 'waiting_sam';
                        waitingSam(context);
                        _messages.add(
                          ItemMessage(
                            userMessage: true,
                            content: _textController.text,
                            type: 'text',
                          ),
                        );
                      });
                      _sendQueryBot(_textController.text, context);
                    },
                    child: Container(
                      margin: EdgeInsets.all(5),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: CustomColors.primary,
                      ),
                      child: Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        : action == 'quickly_answers'
            ? Container(
                decoration: BoxDecoration(
                  color: Color(0xFF7B9EBD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                padding: EdgeInsets.all(12),
                width: MediaQuery.of(context).size.width,
                child: Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 12,
                  runSpacing: 20,
                  direction: Axis.horizontal,
                  children: List<Widget>.generate(
                    options.length,
                        (index) => InkWell(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      onTap: () {
                        setState(() {
                          if (options.first == 'iniciar SAM') {
                            closePanel(context);
                            action = 'waiting_sam';
                            initialPanel = false;
                            waitingSam(context);
                            _messages.add(
                              ItemMessage(
                                userMessage: true,
                                content: options[index],
                                type: 'text',
                              ),
                            );
                          } else {
                            action = 'waiting_sam';
                            waitingSam(context);
                            _messages.add(
                              ItemMessage(
                                userMessage: true,
                                content: options[index],
                                type: 'text',
                              ),
                            );
                          }
                        });
                        _sendQueryBot(options[index], context);
                      },
                      child: new Container(
                        padding:
                        EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Text(
                          options[index],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : Container();
  }
}


import 'package:flutter/material.dart';
import 'package:human_place_app/src/colors.dart';

class LoginScreen extends StatefulWidget {
  static final routerName = '/login-screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/logos/logo-human-place-azul.png',
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                child: Text(
                  'Transformación personal para\nalcanzar todo tu potencial',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'sen-regular',
                      color: CustomColors.primary
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                width: size.width * 0.6,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: CustomColors.primary),
                      borderRadius: new BorderRadius.circular(8),
                    ),
                  ),
                    onPressed: () {},
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 7,
                          horizontal: 5
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                  'assets/icons/google.png',
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 14,),
                          Text(
                              'Iniciar sesión con Google',
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )
                        ],
                      ),
                    ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:human_place_app/src/screens/login_screen.dart';
import 'package:human_place_app/src/widgets/header_intro_slider.dart';

import '../colors.dart';

class IntroSlider extends StatefulWidget {
  static final routerName = '/intro-slider';

  @override
  _IntroSliderState createState() => _IntroSliderState();
}

class _IntroSliderState extends State<IntroSlider> {
  PageController pagViewController = PageController();
  double currentPageIndex = 0.0;
  bool activeDot1 = true;
  bool activeDot2 = false;
  bool activeDot3 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pagViewController,
        onPageChanged: (index) {
          setState(() {
            currentPageIndex = index + .0;
            print(currentPageIndex);
            if (currentPageIndex == 0.0) {
              activeDot1 = true;
              activeDot2 = false;
              activeDot3 = false;
            } else if (currentPageIndex == 1.0) {
              activeDot1 = false;
              activeDot2 = true;
              activeDot3 = false;
            } else {
              activeDot1 = false;
              activeDot2 = false;
              activeDot3 = true;
            }
          });
        },
        children: <Widget>[
          stepOne(context),
          stepTwo(context),
          stepThree(context),
        ],
      ),
    );
  }

  Widget stepOne(BuildContext context) {

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              HeaderIntroSlider(
                color: CustomColors.primary,
                urlIcon: 'assets/icons/icon_onboarding_01.png',
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: Text(
                  'MICRO CONVERSACIÓN',
                  style: TextStyle(
                      fontSize: 27,
                      fontFamily: 'sen-bold',
                      color: CustomColors.orange
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: Text(
                  'Vivir de manera plena enfocado en tu\nbienestar y calidad de vida será la\nprioridad para alcanzar tus metas.',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'sen-regular',
                      fontSize: 16
                  ),
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              dotsNavigation(context),
              SizedBox(height: 50,),
              Container(
                height: 50,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/logos/logo-human-place-azul.png',
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15,)
            ],
          )
        ],
      ),
    );
  }

  Widget stepTwo(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              HeaderIntroSlider(
                color: CustomColors.lightBlue,
                urlIcon: 'assets/icons/icon_onboarding_02.png',
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: Text(
                  'BIBLIOTECA',
                  style: TextStyle(
                      fontSize: 27,
                      fontFamily: 'sen-bold',
                      color: CustomColors.orange
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: Text(
                  'Sesiones de microaprendizaje para\nfortalecer o crear nuevos conocimientos,\npara luego aplicar el pensamiento crítico\ny reflexivo sobre cada temática.',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'sen-regular',
                      fontSize: 16
                  ),
                ),
              ),
            ],
          ),
          Column(
            children: [
              dotsNavigation(context),
              SizedBox(height: 50,),
              Container(
                height: 50,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/logos/logo-human-place-azul.png',
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15,)
            ],
          )
        ],
      ),
    );
  }

  Widget stepThree(BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: [
            HeaderIntroSlider(
              color: CustomColors.purple,
              urlIcon: 'assets/icons/icon_onboarding_03.png',
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Text(
                'RUTINAS',
                style: TextStyle(
                    fontSize: 27,
                    fontFamily: 'sen-bold',
                    color: CustomColors.orange
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Text(
                'Hábitos saludables para poner en práctica\ntus nuevos conocimientos. Midiendo tu\nprogreso e identificar los obstáculos que\nte impiden cristalizar tus aprendizajes\npara ajustar estas rutinas a tu diario vivir.',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontFamily: 'sen-regular',
                    fontSize: 16
                ),
              ),
            ),
          ],
        ),
        Stack(
          children: [
            Column(
              children: <Widget>[
                dotsNavigation(context),
                SizedBox(height: 50,),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/logos/logo-human-place-azul.png',
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15,)
              ],
            ),
            Positioned(
              bottom: 25,
              right: 20,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed(LoginScreen.routerName);
                },
                child: Icon(
                    Icons.arrow_forward,
                    color: CustomColors.orange,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget dotsNavigation(context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                0,
                duration: Duration(seconds: 1),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: EdgeInsets.all(6.5),
              decoration: BoxDecoration(
                color: activeDot1 ? CustomColors.orange : CustomColors.primary,
                shape: BoxShape.circle
              ),
            ),
          ),
          SizedBox(
            width: 6.5,
          ),
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                1,
                duration: Duration(seconds: 1),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: EdgeInsets.all(6.5),
              decoration: BoxDecoration(
                  color: activeDot2 ? CustomColors.orange : CustomColors.primary,
                  shape: BoxShape.circle
              ),
            ),
          ),
          SizedBox(
            width: 6.5,
          ),
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                2,
                duration: Duration(seconds: 1),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: EdgeInsets.all(6.5),
              decoration: BoxDecoration(
                  color: activeDot3 ? CustomColors.orange : CustomColors.primary,
                  shape: BoxShape.circle
              ),
            ),
          ),
        ],
      ),
    );
  }
}

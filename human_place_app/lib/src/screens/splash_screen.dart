import 'package:flutter/material.dart';
import 'package:human_place_app/src/colors.dart';
import 'package:human_place_app/src/screens/chat_screen.dart';
import 'package:human_place_app/src/screens/intro_slider.dart';

class SplashScreen extends StatefulWidget {
  static final routerName = '/';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primary,
      body: FutureBuilder(
        future: timeout(context),
        builder: (context, data) {
          if (data.connectionState == ConnectionState.done) {
            return Container();
          }
          return splash(context);
        },
      ),
    );
  }

  Future<void> timeout(BuildContext context) async {
    await Future.delayed(
      Duration(
        seconds: 5,
      ),
    );
    Navigator.of(context).pushReplacementNamed(IntroSlider.routerName);
  }

  Widget splash(context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: Container(
        height: size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/logos/logo-human-place-blanco.png',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              child: Text(
                  'Transformación personal para\nalcanzar todo tu potencial',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'sen-regular',
                  color: Colors.white
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
